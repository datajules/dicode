import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
import numpy as np
import sys
import chardet
from sklearn.model_selection import KFold
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
import pickle
from Tools import *
from nltk.stem.snowball import SnowballStemmer

"""
Función que permite verificar que los argumentos
introducidos en la terminal sean correctos para la
correcta ejecución del  programa
"""
def verify_args(argvs):
    if len(argvs)==8:
        if argvs[2].split('.')[1] == 'csv':
            filename = argvs[2]
        else:
            print("El archivo no está en formato .csv")
            print(argvs[2].split('.'))
            exit()
        if argvs[4][0] == 'w':
            features = 'word'
            ngram_value = int(argvs[4][1])
        elif argvs[4][0] == 'c':
            features = 'char'
            ngram_value = int(argvs[4][1])
        else:
            print(argvs[4].split(''))
            print("Error, caracteristicas no identificadas")
            exit()
        model_name = argvs[6]
        vectorizer_name = argvs[7]
    else:
        print("Error en los argumentos")
        exit()
    return filename, features, ngram_value, model_name, vectorizer_name

"""
Función para obtener los vectores de características y las etiquetas
de cada ejemplo necesarios para entrenar el sistema.
"""
def get_features(dataframe, features, ngram):
    labelEncoder = LabelEncoder()
    stemmer = SnowballStemmer('spanish')
    texts = [text for text in dataframe['texto']]
    targets = [target for target in dataframe['area_trab']]
    # Preprocesamiento del texto
    texts = normalize_text(texts)
    texts = text2lowercase(texts)
    texts = stemmingTexts(texts,stemmer)
    if features == 'word':
        vectorizer = CountVectorizer(ngram_range=(ngram,ngram))
        x = vectorizer.fit_transform(texts)
    elif features == 'char':
        vectorizer = CountVectorizer(ngram_range=(ngram,ngram), analyzer='char')
        x = vectorizer.fit_transform(texts)
    y = labelEncoder.fit_transform(targets)
    save_models('labelEncoder.pkl',labelEncoder)
    return x,y, vectorizer

"""
Función para guardar los modelos del clasificador,
vectorizador y codificador de etiquetas
"""
def save_models(model_name, model):
    model_file = open(model_name,'wb')
    pickle.dump(model, model_file)

#######################################
#       EJECUCIÓN DEL PROGRAMA        #
#######################################
filename, features, ngram, model_name, vectorizer_name = verify_args(sys.argv)
data = get_data(filename)
x,y, vectorizer = get_features(data, features, ngram)
results = []
classifier = SVC(kernel='linear')

#####################
# Log de evaluación #
#####################
evaluation_file = open('evaluation.txt', 'w', encoding='utf8')
evaluation_file.write('Features: '+ features + ' ngram_value:' + str(ngram)+ '\n')
evaluation_file.write('Model: ' + type(classifier).__name__ +'\n\n')

kfold = KFold(n_splits=10)
index = 1
for train_index, test_index in kfold.split(x,y):
    x_train, x_test = x[train_index], x[test_index]
    y_train, y_test = y[train_index], y[test_index]
    classifier.fit(x_train,y_train)
    y_pred = classifier.predict(x_test)
    accuracy = accuracy_score(y_test, y_pred)
    evaluation_file.write(str(index) + '-fold: Accuracy = ' + str(accuracy) + '\n')
    results.append(accuracy)
    index += 1

evaluation_file.write('\nAverage = {}'.format(np.mean(results)))
save_models(model_name, classifier)
save_models(vectorizer_name, vectorizer)












