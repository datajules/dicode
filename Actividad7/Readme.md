Readme
================

El algoritmo de esta actividad filtra la base de vacantes seleccionando los casos indicados en sus argumentos.

Insumos:
--------

La operación del algoritmo requiere de dos insumos:

1.  El archivo de excel con las categorías del área sobre la que deseamos filtrar. Este archivo se nomina genéricamente *Area\_Categorias.xlsx* y es producto del *Algoritmo6.py* de la actividad 6, correspondiente a la clasificación dinámica.

2.  El archivo en formato csv que contiene la base procesada, sobre la cual se realizará el filtrado.

Argumentos:
-----------

Los argumentos corresponden a cada uno de los campos sobre los que se efectúan los filtros. Los filros se agregan especificando el campo con dos puntos al final y seguido por los números de filas correspondientes al Excel *Area\_Categorias.xlsx* en donde se encuentran. Los campos que deberán ser especificados, en orden son:

-   sexo
-   salario
-   edad
-   horario
-   competencia
-   capacidad
-   experiencia
-   requisitos
-   funciones

En caso de que no se desee aplicar algún filtro en uno de los campos, es necesario colocar el campo, pero no se agregaran números.

Estructura de la función
------------------------

``` bash
python Actividad7.py base_procesada.csv categorias_Area.xlsx sexo: [índices] salario: [índices] edad: [índices] horario: [índices] competencia: [índices] capacidad: [índices] experiencia: [índices] requisitos: [índices] funciones: [índices]
```

En donde:

-   *base\_procesada.csv* -&gt; Corresponde a la base procesada proveniente de las actividades anteriores, y que contiene los campos estructurados.

-   *categorias\_Area.xlsx* -&gt; Corresponde al archivo de Excel que contiene las categorías.

Ejemplo
-------

``` bash
python Actividad7.py xample_processed.csv categorias_Ventas.xlsx sexo: 2 3 salario: 8 145 297 2 edad: 2 3 10 27 6 443 horario: 5 2 6 29 15 328 competencia: 6 4 5 9 12 50 61 47 capacidad: 36 37 50 53 34 12 experiencia: 150 4 24 23 5 requisitos: 7 6 29 funciones: 18
```
