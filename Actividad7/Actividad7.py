# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 16:54:40 2020

@author: jvice
"""
import pandas as pd
import sys
import re
#import pandas as pd
def verify_args(argvs):

    if argvs[1].split('.')[1] == 'csv':
        filename = argvs[1]
    else:
        print("El archivo no está en formato .csv")
        print(argvs[1].split('.'))
        exit()
    area = argvs[2]
    # Falta filtrar por area de acuerdo al archivo (extraer el nombre usando reguex)
    j = 3
    sexo = ''
    for i in range(j+1,len(argvs)):
        j = i
        if argvs[i][len(argvs[i]) - 1] != ':':
            sexo = sexo + ' ' + argvs[i]
        else:
            break
    salario = ''
    for i in range(j+1,len(argvs)):
        j = i
        if argvs[i][len(argvs[i]) - 1] != ':':
            salario = salario + ' ' + argvs[i]
        else:
            break
    edad = ''
    for i in range(j+1,len(argvs)):
        j = i
        if argvs[i][len(argvs[i]) - 1] != ':':
            edad = edad + ' ' + argvs[i]
        else:
            break
    horario = ''
    for i in range(j+1,len(argvs)):
        j = i
        if argvs[i][len(argvs[i]) - 1] != ':':
            horario = horario + ' ' + argvs[i]
        else:
            break
    competencia = ''
    for i in range(j+1,len(argvs)):
        j = i
        if argvs[i][len(argvs[i]) - 1] != ':':
            competencia = competencia + ' ' + argvs[i]
        else:
            break
    capacidad = ''
    for i in range(j+1,len(argvs)):
        j = i
        if argvs[i][len(argvs[i]) - 1] != ':':
            capacidad = capacidad + ' ' + argvs[i]
        else:
            break
    experiencia = ''
    for i in range(j+1,len(argvs)):
        j = i
        if argvs[i][len(argvs[i]) - 1] != ':':
            experiencia = experiencia + ' ' + argvs[i]
        else:
            break
    requisitos = ''
    for i in range(j+1,len(argvs)):
        j = i
        if argvs[i][len(argvs[i]) - 1] != ':':
            requisitos = requisitos + ' ' + argvs[i]
        else:
            break
    funciones = ''
    for i in range(j+1,len(argvs)):
        j = i
        if argvs[i][len(argvs[i]) - 1] != ':':
            funciones = funciones + ' ' + argvs[i]
        else:
            break
    return [filename, area[0:], sexo[1:], salario[1:], edad[1:], horario[1:], competencia[1:], capacidad[1:], experiencia[1:], requisitos[1:], funciones[1:]]
    
filename, area, sexo, salario, edad, horario, competencia, capacidad, experiencia, requisitos, funciones = verify_args(sys.argv)

print('\n')
print('Archivo a filtrar:', filename)
print('Archivo con Filtros:', area)
print('Filtros sexo:', sexo)
print('Filtros salario:', salario)
print('Filtros edad:', edad)
print('Filtros horario:', horario)
print('Filtros competencia:', competencia)
print('Filtros capacidad:', capacidad)
print('Filtros experiencia:', experiencia)
print('Filtros requisitos:', requisitos)
print('Filtros funciones:', funciones)

# Ahora vamos a leer el archivo de categoria

area_categoria = pd.read_excel(area, sheet_name='Sheet1')
base_filtrada = pd.read_csv(filename, engine='python', encoding='utf8')
#base_filtrada2=pd.DataFrame(base_filtrada)
# Ahora usaremos los índices indicados para crear los filtros

sexo=sexo.split()
salario=salario.split()
edad=edad.split()
horario=horario.split()
competencia = competencia.split()
capacidad=capacidad.split()
experiencia=experiencia.split()
requisitos=requisitos.split()
funciones=funciones.split()

area_filtro = re.search("_[A-Za-z ]+", area).group(0)
area_filtro = re.search("[A-Za-z ]+", area_filtro).group(0)
base_filtrada = base_filtrada[base_filtrada['area'] == area_filtro]

filtros = []
if (len(sexo) > 0):
    for i in range(0,len(sexo)):
        j = int(sexo[i])-2
        filtro = area_categoria.sexo.iloc[j]
        filtros.append(filtro)
    base_filtrada = base_filtrada[base_filtrada.sexo.isin(filtros)]
    filtros = []
if (len(salario) > 0):
    for i in range(0,len(salario)):
        j = int(salario[i])-2
        filtro = area_categoria.salario.iloc[j]
        filtros.append(filtro)
    base_filtrada = base_filtrada[base_filtrada.salario.isin(filtros)]
    filtros = []
if (len(edad) > 0):
    for i in range(0,len(edad)):
        j = int(edad[i])-2
        filtro = area_categoria.edad.iloc[j]
        filtros.append(filtro)
    base_filtrada = base_filtrada[base_filtrada.edad.isin(filtros)]
    filtros = []
if (len(horario) > 0):
    for i in range(0,len(horario)):
        j = int(horario[i])-2
        filtro = area_categoria.horario.iloc[j]
        filtros.append(filtro)
    base_filtrada = base_filtrada[base_filtrada.horario.isin(filtros)]
    filtros = []
if (len(competencia) > 0):
    for i in range(0,len(competencia)):
        j = int(competencia[i])-2
        filtro = area_categoria['competencia/habilidad'].iloc[j]
        filtros.append(filtro)
    base_filtrada = base_filtrada[base_filtrada['competencia/habilidad'].isin(filtros)]
    filtros = []
if (len(capacidad) > 0):
    for i in range(0,len(capacidad)):
        j = int(capacidad[i])-2
        filtro = area_categoria.capacidad.iloc[j]
        filtros.append(filtro)
    base_filtrada = base_filtrada[base_filtrada.capacidad.isin(filtros)]
    filtros = []
if (len(experiencia) > 0):
    for i in range(0,len(experiencia)):
        j = int(experiencia[i])-2
        filtro = area_categoria.experiencia.iloc[j]
        filtros.append(filtro)
    base_filtrada = base_filtrada[base_filtrada.experiencia.isin(filtros)]
    filtros = []
if (len(requisitos) > 0):
    for i in range(0,len(requisitos)):
        j = int(requisitos[i])-2
        filtro = area_categoria.requisitos.iloc[j]
        filtros.append(filtro)
    base_filtrada = base_filtrada[base_filtrada.requisitos.isin(filtros)]
    filtros = []
if (len(funciones) > 0):
    for i in range(0,len(funciones)):
        j = int(funciones[i])-2
        filtro = area_categoria.funciones.iloc[j]
        filtros.append(filtro)
    base_filtrada = base_filtrada[base_filtrada.funciones.isin(filtros)]
    
base_filtrada.to_csv("BaseFiltrada.csv", encoding='utf8')
